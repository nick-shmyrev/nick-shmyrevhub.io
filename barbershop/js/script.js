var modal_login = document.querySelector(".modal-login");
var modal_map = document.querySelector(".modal-map");

        var login_btn = document.querySelector(".login");
        var map_btn = document.querySelector(".map-btn");
        var modal_login_close = document.querySelector(".modal-login-close")
        var modal_map_close = document.querySelector(".modal-map-close")

        modal_login.classList.remove("modal-show");
        modal_map.classList.remove("modal-show");

        login_btn.addEventListener("click", function(event) {
          event.preventDefault();
          modal_login.classList.toggle("modal-show")
          modal_map.classList.remove("modal-show")
          console.log("click on login btn");
        });
        map_btn.addEventListener("click", function(event) {
          event.preventDefault();
          modal_map.classList.toggle("modal-show")
          modal_login.classList.remove("modal-show")
          console.log("click on map btn");
        });
        modal_login_close.addEventListener("click", function(event) {
          event.preventDefault();
          modal_login.classList.remove("modal-show")
          console.log("close login");
        });
        modal_map_close.addEventListener("click", function(event) {
          event.preventDefault();
          modal_map.classList.remove("modal-show")
          console.log("close map");
        });
