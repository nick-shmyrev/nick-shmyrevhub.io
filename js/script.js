if (/MSIE 10/i.test(navigator.userAgent)) {
   // this is internet explorer 10
   window.alert('This website is not intended to be viewed in Internet Explorer.\nPlease use Chrome, Firefox, Opera, Safari or any other modern browser');
}

if(/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)){
    // this is internet explorer 9 and 11
    window.alert('This website is not intended to be viewed in Internet Explorer.\nPlease use Chrome, Firefox, Opera, Safari or any other modern browser');
}

if (/Edge\/12./i.test(navigator.userAgent)){
   // this is Microsoft Edge
   window.alert('This website is not intended to be viewed in Edge.\nPlease use Chrome, Firefox, Opera, Safari or any other modern browser');
}

var btn_menu = document.querySelector(".menu");
var header_nav = document.querySelector("header nav");

btn_menu.addEventListener("click", function(){
  header_nav.classList.add("show");
});
window.addEventListener("click", function(event){
  if( !event.target.matches('.menu') && header_nav.classList.contains("show") ){
    header_nav.classList.remove("show");
  };
});
